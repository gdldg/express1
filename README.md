Steps:
* created a directory under express1 called deploy-test:
```mkdir express1/deploy-test```

* Copied the files express1.sh, nginx-redirect.conf, nginx-tls.conf, nginx.conf, updateAndRestart.sh into the new directory, express1/deploy-test.

* Created a new s3 bucket with the app-testing.494914.xyz name

* Created a new cname entry on the google console, pointing to the new bucket
* 
* Added a new routine 'deloyToAWS-test' in gitlab-ci.yaml to check which branch is pushing changes. If the commit ref name is not master, then run deploy/deploy.sh test:```deployToAWS-test:
  #variables:
  stage: deploy
  script:
    - apk add bash
    - apk add openssh-client
    - bash deploy/deploy.sh test
  rules:
    - if: '$CI_COMMIT_REF_NAME != "master"'```

* Modified deploy/deploy.sh to check if argument 1 is master, and if so, call ./deploy-testing/updateAndRestart.sh. Else, call ./deploy/updateAndRestart.sh. This makes use of the argument provided by the gitlab-ci.yaml routine, allowing us to make use of deploy/deploy.sh without the need of duplicating it:
```if [ "$1" === "master" ]; then
    ssh ec2-user@${DEPLOY_SERVER} 'bash' < ./deploy/updateAndRestart.sh
else
    ssh ec2-user@${DEPLOY_SERVER} 'bash' < ./deploy-test/updateAndRestart.sh 
fi
```

* Modified the instances under the scripts express1.sh, updateAndRestart.sh, nginx-redirect.conf, nginx-tls.conf, and nginx.conf that pointed to app.494914.xyz to now point to app-test.494914.xyz. 

* Ran deploy/certbot.sh again, now creating a certificate for app-test.494914.xyz to provide TLS for the app testing domain. 

* Added the Gitlab CI/CD aws credentials to variables required on the repository for automatic deloyment.

* SSH'd into the ec2 instance server, and copied the nginx-tls.con, and nginx-redirect.conf to the ~/etc/nginx/conf.d/ directory.

* Note: Surely, there must be a more effective way of passing arguments around before the ssh into the server step, such as ```ssh ec-2user@${DEPLOY_SERVER} 'bash' < ./deploy/updateAndRestart.sh test```, which could then cascade to the rest of the scripts. Unfortunately, I was not able to find an effective way to do this. Will revisit.
