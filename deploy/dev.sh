#!/bin/bash
docker run --rm -it \
        -v $PWD:/mnt/stuff \
        -p 8080:3000 \
        -w /mnt/stuff node:alpine npm run firststart
